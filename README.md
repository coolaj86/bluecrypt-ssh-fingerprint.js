# Bluecrypt SSH Fingerprint (for Browsers)

A minimal library to read an SSH public key (`id_rsa.pub`)
and generate its fingerprint.

Works for RSA and ECDSA public keys.

# Features

&lt; 150 lines of code | 1.1kb gzipped | 2.4kb minified | 4.1kb with comments

* [x] SSH Public Key SHA256 Fingerprints
  * RSA (2048-bit, 3072-bit, 4096-bit)
  * EC / ECDSA
    * P-256 (prime256v1, secp256r1)
    * P-384 (secp384r1)
* [x] Node.js version
  * [Greenlock SSH Fingerprint](https://git.coolaj86.com/coolaj86/ssh-fingerprint.js)

### Need SSH Private Keys?

SSH private keys (`id_rsa`) are just normal PEM files,
so you can use Eckles or Rasha, as mentioned above.

# Web Demo

<https://coolaj86.com/demos/ssh-fingerprint/>

<img border="1" src="https://git.coolaj86.com/coolaj86/bluecrypt-ssh-fingerprint.js/raw/branch/master/screenshot.png" />

```bash
git clone https://git.coolaj86.com/coolaj86/bluecrypt-ssh-fingerprint.js
pushd bluecrypt-ssh-fingerprint.js/
```

```bash
open index.html
```

# Install

You can use it as a plain-old javascript library:

```html
<script src="https://git.coolaj86.com/coolaj86/bluecrypt-ssh-fingerprint.js/raw/branch/master/ssh-fingerprint.js"></script>
```

It's also on npm:

```bash
npm install bluecrypt-ssh-fingerprint
```

# Usage

Very simple:

```js
var pub = 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCba21UHE+VbDTpmYYFZUOV+OQ8AngOCdjROsPC0KiEfMvEaEM3NQl58u6QL7G7QsErKViiNPm9OTFo6HF5JijfWzK7haHFuRMEsgI4VwIYyhvqlJDfw/wt0AiVvSmoMfEQn1p1aiaO4V/RJSE3Vw/uz2bxiT22uSkSqOyShyfYE6dMHnuoBkzr4jvSifT+INmbv6Nyo4+AAMCZtYeHLrsFeSTjLL9jMPjI4ZkVdlw2n3Xn9NbltF3/8Ao8dQfElqw+LIQWqU0oFHYNIP4ttfl5ObMKHaKSvBMyNruZR0El/ZsrcHLkAHRCLj07KRQJ81l5CUTPtQ02P1Eamz/nT4I3 rsa@localhost';

SSH.fingerprint({ pub: pub }).then(function (fing) {
  console.info('The key fingerprint is:');
  console.info(fing.fingerprint);
});
```

# Other Tools in the Bluecrypt Suite

* [Browser JWK to SSH](https://git.coolaj86.com/coolaj86/bluecrypt-jwk-to-ssh.js)
* [Browser SSH to JWK](https://git.coolaj86.com/coolaj86/bluecrypt-ssh-to-jwk.js)
* [Browser ASN.1 parser](https://git.coolaj86.com/coolaj86/asn1-parser.js) (for x509, etc)
* [Browser ASN.1 packer](https://git.coolaj86.com/coolaj86/asn1-packer.js) (for x509, etc)

# Legal

[ssh-fingerprint.js](https://git.coolaj86.com/coolaj86/bluecrypt-ssh-fingerprint.js) |
MPL-2.0 |
[Terms of Use](https://therootcompany.com/legal/#terms) |
[Privacy Policy](https://therootcompany.com/legal/#privacy)

Bluecrypt&trade; is owned by AJ ONeal
